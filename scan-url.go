package main

import (
	"bufio"
	"fmt"
	"net/http"
	"os"
	"time"
)

func main() {
	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Enter url: ")
		text, _ := reader.ReadString('\n')

		ticker := time.NewTicker(100 * time.Millisecond)
		done := make(chan bool)

		go func() {
			for {
				select {
				case <-done:
					return
				case t := <-ticker.C:
					fmt.Println("Tick at", t)
				}
			}
		}()

		resp, err := http.Get("https://google.com")
		//resp, err := http.Get(text)

		if err != nil {
			panic(err)
		}
		resp.Body.Close()



		fmt.Println(text)
		fmt.Println(resp.Body)
		done <- true
	}
}
